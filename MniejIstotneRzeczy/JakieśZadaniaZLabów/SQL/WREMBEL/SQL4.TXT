
SQL*Plus: Release 8.0.5.0.0 - Production on �ro Mar 29 11:52:15 2000

(c) Copyright 1998 Oracle Corporation.  All rights reserved.


Po��czony z:
Oracle8 Enterprise Edition Release 8.0.4.0.0 - Production
With the Partitioning and Objects options
PL/SQL Release 8.0.4.0.0 - Production

SQL> select nazwisko from pracownicy where placa_pod=(select min(placa_pod) from pracownicy);

NAZWISKO
---------------
ZAKRZEWICZ

SQL> select nazwisko from pracownicy where placa_pod=(select min(placa_pod) from pracownicy group by
 id zesp);
select nazwisko from pracownicy where placa_pod=(select min(placa_pod) from pracownicy group by id z
                                                                                                   *
B��D w linii 1:
ORA-00907: brak prawego nawiasu


SQL> select nazwisko from pracownicy
  2  where placa_pod=(select min(placa_pod) from pracownicy group by id_zesp);
where placa_pod=(select min(placa_pod) from pracownicy group by id_zesp)
                 *
B��D w linii 2:
ORA-01427: jednowierszowe podzapytanie zwraca wi�cej ni� jeden wiersz


SQL> select max(avg(placa_pod)) from pracownicy group by id_zesp;

MAX(AVG(PLACA_POD))
-------------------
               1350

SQL> select nazwisko from pracownicy s where not exists (select * from pracownicy p where s.id_prac=
p.id_szefa);

NAZWISKO
---------------
KROLIKOWSKI
KOSZLAJDA
JEZIERSKI
MATYSIAK
MAREK
ZAKRZEWICZ
BIALY
KONOPKA
HAPKE

9 wierszy zosta�o wybranych.

SQL> select nazwisko,p.placa_pod-srednia as roznica
  2  from pracownicy p,(select avg(placa_pod) from pracownicy) pp
  3  where p.placa_pod>pp;
where p.placa_pod>pp
                  *
B��D w linii 3:
ORA-00904: niepoprawna nazwa kolumny


SQL> select avg(placa_pod) from pracownicy;

AVG(PLACA_POD)
--------------
     701,02857

SQL> 1
  1* select avg(placa_pod) from pracownicy
SQL> 2
B��dny numer linii
SQL> 3
B��dny numer linii
SQL> select nazwisko,p.placa_pod-srednia as roznica
  2  from pracownicy p,(select avg(placa_pod) as srednia from pracownicy)
  3  where p.placa>srednia;
where p.placa>srednia
        *
B��D w linii 3:
ORA-00904: niepoprawna nazwa kolumny


SQL> 3
  3* where p.placa>srednia
SQL> c/p.placa/p.placa_pod
  3* where p.placa_pod>srednia
SQL> r
  1  select nazwisko,p.placa_pod-srednia as roznica
  2  from pracownicy p,(select avg(placa_pod) as srednia from pracownicy)
  3* where p.placa_pod>srednia

NAZWISKO          ROZNICA
--------------- ---------
WEGLARZ         1028,9714
BLAZEWICZ       648,97143
SLOWINSKI       368,97143
BRZEZINSKI      258,97143
MORZY           128,97143

SQL> select nazwisko,etat,placa_pod
  2  from pracownicy
  3  where placa_pod=(select max(placa_pod)
  4  from pracownicy);

NAZWISKO        ETAT       PLACA_POD
--------------- ---------- ---------
WEGLARZ         DYREKTOR        1730

SQL> select nazwisko,etat,placa_pod
  2  from pracownicy
  3  where (placa_pod,etat) in (select max(placa_pod),etat
  4  from pracownicy);
where (placa_pod,etat) in (select max(placa_pod),etat
                                                 *
B��D w linii 3:
ORA-00937: to nie jest jednogrupowa funkcja grupowa


SQL> select nazwisko,etat,placa_pod from pracownicy
  2  where (placa_pod,etat) in (select max(placa_pod),etat from pracownicy group by etat);

NAZWISKO        ETAT       PLACA_POD
--------------- ---------- ---------
BIALY           STAZYSTA         250
MAREK           SEKRETARKA     410,2
KONOPKA         ASYSTENT         480
HAPKE           ASYSTENT         480
KROLIKOWSKI     ADIUNKT        645,5
BLAZEWICZ       PROFESOR        1350
WEGLARZ         DYREKTOR        1730

7 wierszy zosta�o wybranych.

SQL> select * from pracownicy;

  ID_PRAC NAZWISKO        ETAT        ID_SZEFA ZATRUDNI PLACA_POD PLACA_DOD   ID_ZESP
--------- --------------- ---------- --------- -------- --------- --------- ---------
      100 WEGLARZ         DYREKTOR             68/01/01      1730     420,5        10
      110 BLAZEWICZ       PROFESOR         100 73/05/01      1350       210        40
      120 SLOWINSKI       PROFESOR         100 77/09/01      1070                  30
      130 BRZEZINSKI      PROFESOR         100 68/07/01       960                  20
      140 MORZY           PROFESOR         130 75/09/15       830       105        20
      150 KROLIKOWSKI     ADIUNKT          130 77/09/01     645,5                  20
      160 KOSZLAJDA       ADIUNKT          130 85/03/01       590                  20
      170 JEZIERSKI       ASYSTENT         130 92/11/01     439,7      80,5        20
      190 MATYSIAK        ASYSTENT         140 93/09/01       371                  20
      180 MAREK           SEKRETARKA       100 85/02/20     410,2                  10
      200 ZAKRZEWICZ      STAZYSTA         140 94/06/15       208                  30
      210 BIALY           STAZYSTA         130 93/10/15       250     170,6        30
      220 KONOPKA         ASYSTENT         110 93/10/01       480                  20
      230 HAPKE           ASYSTENT         120 92/09/01       480        90        30

14 wierszy zosta�o wybranych.

SQL> select id_zesp,nazwisko,zatrudiony from pracownicy
  2  where zatrudniony like (select max(zatrudiony) from pracownicy);
where zatrudniony like (select max(zatrudiony) from pracownicy)
                        *
B��D w linii 2:
ORA-00936: brak wyra�enia


SQL> 2
  2* where zatrudniony like (select max(zatrudiony) from pracownicy)
SQL> c/like/=/
  2* where zatrudniony = (select max(zatrudiony) from pracownicy)
SQL> r
  1  select id_zesp,nazwisko,zatrudiony from pracownicy
  2* where zatrudniony = (select max(zatrudiony) from pracownicy)
select id_zesp,nazwisko,zatrudiony from pracownicy
                        *
B��D w linii 1:
ORA-00904: niepoprawna nazwa kolumny


SQL> c/zatrudiony/zatrudniony/
  1* select id_zesp,nazwisko,zatrudniony from pracownicy
SQL> r
  1  select id_zesp,nazwisko,zatrudniony from pracownicy
  2* where zatrudniony = (select max(zatrudiony) from pracownicy)
where zatrudniony = (select max(zatrudiony) from pracownicy)
                                *
B��D w linii 2:
ORA-00904: niepoprawna nazwa kolumny


SQL> 2
  2* where zatrudniony = (select max(zatrudiony) from pracownicy)
SQL> c/zatrudiony/zatrudniony/
  2* where zatrudniony = (select max(zatrudniony) from pracownicy)
SQL> r
  1  select id_zesp,nazwisko,zatrudniony from pracownicy
  2* where zatrudniony = (select max(zatrudniony) from pracownicy)

  ID_ZESP NAZWISKO        ZATRUDNI
--------- --------------- --------
       30 ZAKRZEWICZ      94/06/15

SQL> 2
  2* where zatrudniony = (select max(zatrudniony) from pracownicy)
SQL> 1
  1* select id_zesp,nazwisko,zatrudniony from pracownicy
SQL> select id_zesp,nazwisko,zatrudniony from pracownicy
  2  where (nazwisko,zatrudniony) in (select nazwisko,max(zatrudniony) from pracownicy);
where (nazwisko,zatrudniony) in (select nazwisko,max(zatrudniony) from pracownicy)
                                        *
B��D w linii 2:
ORA-00937: to nie jest jednogrupowa funkcja grupowa


SQL> select id_zesp,nazwisko,zatrudniony from pracownicy
  2  where (id_zesp,zatrudniony)in(select id_zesp,max(zatrudniony) from pracownicy);
where (id_zesp,zatrudniony)in(select id_zesp,max(zatrudniony) from pracownicy)
                                     *
B��D w linii 2:
ORA-00937: to nie jest jednogrupowa funkcja grupowa


SQL> 2
  2* where (id_zesp,zatrudniony)in(select id_zesp,max(zatrudniony) from pracownicy)
SQL> c/cy)/cy group by id_zesp)
  2* where (id_zesp,zatrudniony)in(select id_zesp,max(zatrudniony) from pracownicy group by id_zesp)
SQL> r
  1  select id_zesp,nazwisko,zatrudniony from pracownicy
  2* where (id_zesp,zatrudniony)in(select id_zesp,max(zatrudniony) from pracownicy group by id_zesp)

  ID_ZESP NAZWISKO        ZATRUDNI
--------- --------------- --------
       10 MAREK           85/02/20
       20 KONOPKA         93/10/01
       30 ZAKRZEWICZ      94/06/15
       40 BLAZEWICZ       73/05/01

SQL> select id_zesp,nazwisko,min(placa_pod) from pracownicy group by id_zesp;
select id_zesp,nazwisko,min(placa_pod) from pracownicy group by id_zesp
               *
B��D w linii 1:
ORA-00979: to nie jest wyra�enie GROUP BY


SQL> select id_zesp,min(placa_pod) from pracownicy group by id_zesp;

  ID_ZESP MIN(PLACA_POD)
--------- --------------
       10          410,2
       20            371
       30            208
       40           1350

SQL> select s.id_zesp,s.nazwisko
  2  from pracownicy s,pracownicy p
  3  where p.id_szefa=s.id_prac
  4  and (id_zesp,p.placa_pod) in (select id_zesp,min(placa_pod)
  5  from pracownicy group by id_zesp);
and (id_zesp,p.placa_pod) in (select id_zesp,min(placa_pod)
     *
B��D w linii 4:
ORA-00918: kolumna zdefiniowana w spos�b niejednoznaczny


SQL> 4
  4* and (id_zesp,p.placa_pod) in (select id_zesp,min(placa_pod)
SQL> c/(id_zesp/(p.id_zesp/
  4* and (p.id_zesp,p.placa_pod) in (select id_zesp,min(placa_pod)
SQL> r
  1  select s.id_zesp,s.nazwisko
  2  from pracownicy s,pracownicy p
  3  where p.id_szefa=s.id_prac
  4  and (p.id_zesp,p.placa_pod) in (select id_zesp,min(placa_pod)
  5* from pracownicy group by id_zesp)

  ID_ZESP NAZWISKO
--------- ---------------
       10 WEGLARZ
       20 MORZY
       20 MORZY
       10 WEGLARZ

SQL> 1
  1* select s.id_zesp,s.nazwisko
SQL> c/s.nazwisko/s.nazwisko,p.nazwisko/
  1* select s.id_zesp,s.nazwisko,p.nazwisko
SQL> r
  1  select s.id_zesp,s.nazwisko,p.nazwisko
  2  from pracownicy s,pracownicy p
  3  where p.id_szefa=s.id_prac
  4  and (p.id_zesp,p.placa_pod) in (select id_zesp,min(placa_pod)
  5* from pracownicy group by id_zesp)

  ID_ZESP NAZWISKO        NAZWISKO
--------- --------------- ---------------
       10 WEGLARZ         MAREK
       20 MORZY           MATYSIAK
       20 MORZY           ZAKRZEWICZ
       10 WEGLARZ         BLAZEWICZ

SQL> select id_zesp,nazwisko
  2  from pracownicy
  3  where (id_prac) in (select id_szefa,min(placa_pod) from pracownicy group by id_zesp);
where (id_prac) in (select id_szefa,min(placa_pod) from pracownicy group by id_zesp)
                    *
B��D w linii 3:
ORA-00913: za du�a liczba warto�ci


SQL> select id_zesp,nazwisko
  2  from pracownicy
  3  where id_prac in (select id_szefa,min(placa_pod) from pracownicy group by id_zesp);
where id_prac in (select id_szefa,min(placa_pod) from pracownicy group by id_zesp)
                  *
B��D w linii 3:
ORA-00913: za du�a liczba warto�ci


SQL> 