#qpy:console

# original script by M. Buszkiewicz
# qpython compatibility by J. Wozniak
import re
import sys
from pygraph.classes.digraph import digraph
from pygraph.algorithms.cycles import find_cycle

# Returns the set of data processed by the set of transactions.
def data_set(realization):
	m = re.findall('[a-z][0-9][a-z0-9]', realization)
	m = set([x[2] for x in m])
	return m

# Returns the set of transactions taking part in a realization.
def transaction_set(realization):
	return set([x[1] for x in operations_exploded(realization)])

def operations_exploded(realization):
	return realization.split()

def recoverable(realization):
	operations = operations_exploded(realization)

	data_modified_by = {x: set() for x in data_set(realization)}

	# go through all operations from the beginning
	for operation in operations:
		op_type = operation[0]
		op_tran_id = operation[1]

		if op_type == 'w':
			op_data = operation[2]
			# transaction writes to data, lets update data_modified_by
			data_modified_by[op_data].add(op_tran_id)

		elif op_type == 'r':
			op_data = operation[2]
			# transaction reads from data, if its been modified by other transaction
			# check if the other's commit is before ours
			touchers = data_modified_by[op_data]
			for toucher in touchers:
				toucher_commit_after_mine = operations.index('c' + toucher) > operations.index('c' + op_tran_id)
				if toucher != op_tran_id and toucher_commit_after_mine:
					print '\tRC: FALSE, because c%s is not before c%s' % (toucher, op_tran_id)
					return False


		# print ''
	
	print '\tRC: TRUE'
	return True

def avoids_cascading_aborts(realization):
	operations = operations_exploded(realization)

	data_modified_by = {x: set() for x in data_set(realization)}

	# go through all operations from the beginning
	for i in range(len(operations)):
		operation = operations[i]
		op_type = operation[0]
		op_tran_id = operation[1]

		if op_type == 'w':
			op_data = operation[2]
			# transaction writes to data, lets update data_modified_by
			data_modified_by[op_data].add(op_tran_id)

		elif op_type == 'r':
			op_data = operation[2]
			# transaction reads from data, if its been modified by other transaction
			touchers = data_modified_by[op_data]
			for toucher in touchers:
				toucher_commit_after_read = operations.index('c' + toucher) > i
				if toucher != op_tran_id and toucher_commit_after_read:
					print '\tACA: FALSE, because c%s is not before r%s%s' % (toucher, op_tran_id, op_data)
					return False


		# print ''
	
	print '\tACA: TRUE'
	return True

def strict(realization):
	operations = operations_exploded(realization)

	data_modified_by = {x: set() for x in data_set(realization)}
	finished_transactions = set()

	# go through all operations from the beginning
	for i in range(len(operations)):
		operation = operations[i]
		op_type = operation[0]
		op_tran_id = operation[1]

		if op_type in ('c', 'a'):
			finished_transactions.add(op_tran_id)

		elif op_type in ('w', 'r'):
			op_data = operation[2]
			if op_type == 'w':
				data_modified_by[op_data].add(op_tran_id)
			for toucher in data_modified_by[op_data]:
				if toucher != op_tran_id and toucher not in finished_transactions:
					print '\tST: FALSE, because T%s is not finished before %s' % (toucher, operation)
					return False
	
	print '\tST: TRUE'
	return True

def conflict_serializable(realization):
	operations = operations_exploded(realization)
	# graph = {x: set() for x in transaction_set(realization)}
	g = digraph()
	g.add_nodes(transaction_set(realization))
	
	# for each...
	for i in range(len(operations)):
		operation = operations[i]
		op_type = operation[0]
		op_tran_id = operation[1]
		if op_type in ('w', 'r'):
			op_data = operation[2]
		else: continue

		# for each pair of operations...
		for j in range(i+1, len(operations)):
			operation2 = operations[j]
			op2_type = operation2[0]
			op2_tran_id = operation2[1]
			if op2_type in ('w', 'r'):
				op2_data = operation2[2]
			else: continue

			# 1. same data
			# 2. different transactions
			# 3. at least one is 'w'
			# if all conditions fulfilled, add arc from T1 to T2
			if op_data == op2_data and op_tran_id != op2_tran_id and (op_type == 'w' or op2_type == 'w'):
				# print operation, 'precedes', operation2, ', adding arc from T%s to T%s' % (op_tran_id, op2_tran_id)
				# graph[op_tran_id].add(op2_tran_id)
				if not g.has_edge((op_tran_id, op2_tran_id)):
					g.add_edge((op_tran_id, op2_tran_id))

	# now look at the graph and find if there's any cycle
	cyc = find_cycle(g)
	if len(cyc) == 0:
		print '\tCSR: TRUE (no cycle)'
		return True
	else:
		print '\tCSR: FALSE (cycle: %s)' % cyc
		return False

if __name__ == '__main__':
	r = raw_input()
	print 'Analysis of realization:', r
	recoverable(r)
	avoids_cascading_aborts(r)
	strict(r)
	conflict_serializable(r)
