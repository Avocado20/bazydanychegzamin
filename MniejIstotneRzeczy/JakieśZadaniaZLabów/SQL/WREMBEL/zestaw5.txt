#1
create table projekty
(
id_projektu number(4) primary key,
opis_projektu varchar2(20),
data_rozpoczecia date default sysdate,
data_zakonczenia date,
fundusz number(7) constraint fundusz_chk check(fundusz>0),
constraint data_chk check(data_rozpoczecia<data_zakonczenia)
);

create table przydzialy
(
id_projektu number(4) constraint idproj_nn not null,
nr_pracownika number(4) constraint nrprac_nn not null,
od date default sysdate,
do date,
stawka number(7) constraint stawka_chk check(stawka>0),
rola varchar2(12) constraint rola_chk check(
		rola in ('KIERUJACY','ANALITYK','PROGRAMISTA')),
constraint oddo_chk check(od<do),
constraint idproj_fk foreign key (id_projektu) references projekty(id_projektu),
constraint nrprac_fk foreign key (nr_pracownika) references pracownicy(id_prac),
primary key (id_projektu,nr_pracownika)
);

#2
alter table projekty
add godziny number(2);

#3
alter table projekty
modify fundusz number(10);

#4
comment on table projekty is 'To sa projekty';
comment on table przydzialy is 'To sa przydzialy';

#5
comment on column projekty.id_projektu is 'Identyfikator projektu';

#6
select table_name from user_tables;

#7
select table_name as nazwa_tablicy,
column_name as nazwa_kolumny,

#8sam sobie niech szuka...