select nazwisko,nazwa
from pracownicy p,zespoly z
where p.id_zesp(+)=z.id_zesp;
//--------------------------------------------
select id_zesp,nazwisko
from pracownicy p
where placa_pod = 	(select min(placa_pod)
			from pracownicy
			where id_zesp=p.id_zesp)
order by id_zesp;
//--------------------------------------------
Znalezc zespol ktory srednio wyplaca pracownikom najwiecej pieniedzy

select nazwa
from zespoly
where id_zesp in (select id_zesp
		from pracownicy
		group by id_zesp
		having avg(placa_pod) in (select max(avg(placa_pod))
					from pracownicy
					group by id_zesp));
//--------------------------------------------
Znalezc pracownikow zarabiajacych wiecej niz srednia w ich zespole

select id_zesp,nazwisko,placa_pod
from pracownicy p
where placa_pod>(select avg(placa_pod)
		from pracownicy
		where id_zesp=p.id_zesp);

//--------------------------------------------
Znalezc pracownikow nie bedacych szefami

select nazwisko
from pracownicy s
where not exists (select nazwisko
		from pracownicy p
		where p.id_szefa=s.id_prac);
//--------------------------------------------
Zwiekszyc place o 10% sredniej placy w danym zespole

update pracownicy d
set (placa_pod,placa_dod)=
		(select 0.1*avg(p.placa_pod)+d.placa_pod,
		0.15*avg(nvl(placa_dod,0))+d.placa_dod
		from pracownicy p
		where id_zesp=p.id_zesp)
where placa_pod=(select min(placa_pod)
		from pracownicy
		where etat=d.etat);
//--------------------------------------------
Usunac profesorow bez podwladnych

delete from pracownicy p
where not exists (select nazwisko
		from pracownicy
		where id_szefa=p.id_prac)
and p.etat like 'PROFESOR';
//--------------------------------------------
Stworzyc perspektywe liczebnosci zespolow

create view liczebnosc
as select p.id_zesp,nazwa,count(id_prac) as liczebnosc
   from pracownicy p,zespoly z
   where p.id_zesp=z.id_zesp
   group by p.id_zesp,nazwa;
//--------------------------------------------


